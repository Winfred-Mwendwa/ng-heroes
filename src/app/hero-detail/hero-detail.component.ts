import { Component, Input } from '@angular/core'; /*ammended import statement to include the input symbol*/
import { Hero } from 'src/app/hero';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'; /*Angular service for interacting with the browser;lets you navigate back to the previous view. */

import { HeroService } from '../hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.sass']
})
export class HeroDetailComponent {
  @Input() hero?: Hero; /*hero property which is an input property of type Hero*/

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getHero();
  }
  
  /*Route parameters are always strings. 
  The JavaScript Number function converts the string to a number, which is what a hero id should be. 

  route.snapshot is a static image of the route information shortly after the component was created.

  The paramMap is a dictionary of route parameter values extracted from the URL. 
  The "id" key returns the id of the hero to fetch.

*/
  getHero(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.heroService.getHero(id)
      .subscribe(hero => this.hero = hero);
  }

  /*goBack() method that navigates backward one step in the browser's history stack using the Location service that you used to inject. */
  goBack(): void {
    this.location.back();
  }

  save(): void {
    if (this.hero) {
      this.heroService.updateHero(this.hero)
        .subscribe(() => this.goBack());
    }
  }
}
