import { Component } from '@angular/core';
import { MessageService } from '../messages.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent {
  constructor(public messageService: MessageService) {} /*must be public because you will bind to it in the template*/
}
